import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.*;
import java.util.HashMap;
import java.util.Map;

public class Main {
    //inizializzo un oggetto di tipo logger da log4j, lo suo in sostituzione del sout
    protected static final Logger logger = LogManager.getLogger();

    public static void main(String[] args) throws ClassNotFoundException, SQLException, InterruptedException, IOException, NoSuchAlgorithmException {

        //creo la stringa che rappresenta l'URL di connessione al DB in formato jdbc
        String url = "jdbc:mysql://localhost:3306/auto_git";
        String username = "root"; //username db
        String password = "root"; //password db
        Class.forName("com.mysql.cj.jdbc.Driver"); //Carico il driver del db

        //con un try with resource inizializzo la connessione al db, creando un oggetto di tipo Connection
        //uso il try with resource per chiudere la connessione in automatico
        try(Connection con = DriverManager.getConnection(url, username, password)) {
            logger.info("Sono connesso al db !");

            //creo un oggetto mappa che conterra' i valori da leggere di file.name e sha256(checksum).
            Map<String, String> mapFile = new HashMap<>();

            //prendo il path della mia cartella
            final File folder = new File("C:\\Users\\sdiblasi\\OneDrive - NTT DATA EMEAL\\Desktop\\esercizio git");
            //faccio un ciclo infinito
            while (true) {
                logger.info("Ho cominciato il ciclo");
                //ciclo ogni file all'interno della cartella
                for (File fileEntry : folder.listFiles()) {
                    logger.info("Sto processando il file " + fileEntry.getName());
                    //verifico se il file e' un file e non è! la cartella di git...
                    if (fileEntry.isFile() && !fileEntry.getName().equals(".git")) {
                        //verifico se il file esiste nella mappa
                        if (mapFile.containsKey(fileEntry.getName())) {
                            String savedSha = mapFile.get(fileEntry.getName()); // recupero la sha(checksum) precedentemente salvata
                            String newSha = sha256(fileEntry.getAbsolutePath()); // sha(checksum) attuale del file

                            //avendo negato la condizione con "!" andremo a controllare se il file ha subito una modifica.
                            if(!savedSha.equals(newSha)){
                                logger.info("Il file è stato modificato!"); //logger di "avvertimento" di modifica del file

                                mapFile.put(fileEntry.getName(),newSha); //aggiorno la mia mappa con la nuova cifratura.

                                gestioneGitDb(fileEntry, folder, con);
                            }
                        //se il file non è presente nella mia mappa (e non è mai stato computato) allora....
                        } else {
                            //creo una stringa di checksum per il contenuto del file
                            String sha256 = sha256(fileEntry.getAbsolutePath());
                            //prendo la mia mappa ed inserisco al suo interno il nome del file e il suo checksum
                            mapFile.put(fileEntry.getName(), sha256);

                            logger.info("Ho aggiunto il file: " + fileEntry.getName() + " con il checksum: " + sha256);

                            gestioneGitDb(fileEntry, folder, con);
                        }
                    }
                }
                Thread.sleep(20000); //usare un timer è più consono!
            }
        }
    }
    //metodo che contiene tutte le operazioni git e db, dunque sopra per essere più ordinati li richiameremo direttamente così
    private static void gestioneGitDb(File fileEntry, File folder, Connection con) throws SQLException {
        gitPull(folder);

        //uso i vari metodi per aggiornare sia git che db.
        logger.info("Sto eseguendo l'add del file " + fileEntry.getName());
        gitAdd(folder);
        insertAddDb(con);

        logger.info("Sto eseguendo la commit del file " + fileEntry.getName());
        gitCommit(folder);
        insertCommitDb(con);

        logger.info("Sto eseguendo il push del file " + fileEntry.getName());
        gitPush(folder);
        insertPushDb(con);
    }

    //creo un metodo per calcolare la sha256 dato il path di un dato file
    public static String sha256 (String input) throws IOException, NoSuchAlgorithmException {
        Path filePath = Path.of(input);  //creo il path
        byte[] data = Files.readAllBytes(filePath); //leggo i byte del file
        byte[] hash = MessageDigest.getInstance("SHA-256").digest(data); //hasho i byte del file
        return new BigInteger(1, hash).toString(16); //genero una stringa di checksum
    }

    //METODI GIT//

    //creo un metodo per eseguire il git add
    public static void gitAdd(File folder) {
        try{
            String line = "git add ."; //creo la stringa da eseguire
            CommandLine cmdLine = CommandLine.parse(line); //traduco la stringa in commandLine
            DefaultExecutor executor = DefaultExecutor.builder().setWorkingDirectory(folder).get(); //creo un executor
            executor.execute(cmdLine); //eseguo il comando
        } catch (Exception e) {
            logger.warn("Error durante l'add " + e.getMessage());
        }
    }
    //creo un metodo per eseguire il git commit
    public static void gitCommit(File folder){
        try{
            String line = "git commit -m \"auto commit\""; //creo la stringa da eseguire
            CommandLine cmdLine = CommandLine.parse(line); //traduco la stringa in commandLine
            DefaultExecutor executor = DefaultExecutor.builder().setWorkingDirectory(folder).get(); //creo un executor
            executor.execute(cmdLine); //eseguo il comando
        }  catch (Exception e) {
            logger.warn("Error durante il commit " + e.getMessage());
        }
    }
    //creo un metodo per eseguire il git push
    public static void gitPush(File folder){
        try {
            String line = "git push --set-upstream origin main"; //creo la stringa da eseguire
            CommandLine cmdLine = CommandLine.parse(line); //traduco la stringa in commandLine
            DefaultExecutor executor = DefaultExecutor.builder().setWorkingDirectory(folder).get(); //creo un executor
            executor.execute(cmdLine); //eseguo il comando
        } catch (Exception e) {
            logger.warn("Error durante il push " + e.getMessage());
        }
    }
    //creo un metodo per eseguire il git pull
    public static void gitPull(File folder) {
        try {
            String line = "git pull"; //creo la stringa da eseguire
            CommandLine cmdLine = CommandLine.parse(line); //traduco la stringa in commandLine
            DefaultExecutor executor = DefaultExecutor.builder().setWorkingDirectory(folder).get(); //creo un executor
            executor.execute(cmdLine); //eseguo il comando
        } catch (Exception e) {
            logger.warn("Error durante il pull " + e.getMessage());
        }
    }
    //METODI DB//
    public static void insertAddDb (Connection con) throws SQLException {
        //creo un oggetto di tipo Statement che servirà per eseguire le query
        try(Statement st = con.createStatement()){
            String query = "insert into log_entry (orario, tipo_operazione) values (CURRENT_TIMESTAMP(),'add');"; //creo una stringa contenente la query da eseguire
            st.executeUpdate(query); //eseguo la query e la metto in un oggetto di tipo ResultSet
        }
    }
    public static void insertCommitDb (Connection con) throws SQLException {
        //creo un oggetto di tipo Statement che servirà per eseguire le query
        try(Statement st = con.createStatement()){
            String query = "insert into log_entry (orario, tipo_operazione) values (CURRENT_TIMESTAMP(),'commit');"; //creo una stringa contenente la query da eseguire
            st.executeUpdate(query); //eseguo la query e la metto in un oggetto di tipo ResultSet
        }
    }
    public static void insertPushDb (Connection con) throws SQLException {
        //creo un oggetto di tipo Statement che servirà per eseguire le query
        try(Statement st = con.createStatement()){
            String query = "insert into log_entry (orario, tipo_operazione) values (CURRENT_TIMESTAMP(),'push');"; //creo una stringa contenente la query da eseguire
            st.executeUpdate(query); //eseguo la query e la metto in un oggetto di tipo ResultSet
        }
    }
}
